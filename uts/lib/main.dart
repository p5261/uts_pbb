import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Biodata Diri',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: Biodata(),
    );
  }
}

class Biodata extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue,
      body: ListView(
        physics: BouncingScrollPhysics(),
        children: [
          Container(
            margin: EdgeInsets.all(20.0),
            height: 200.0,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage('images/profil.jpg'),
                fit: BoxFit.contain,
              ),
            ),
          ),
          SizedBox(
            child: Divider(),
          ),
          Container(
            alignment: Alignment.center,
            child: Text(
              'BIODATA',
              style: TextStyle(
                fontFamily: 'Source Sans Pro',
                color: Colors.white,
                fontSize: 50.0,
              ),
            ),
          ),
          Card(
            margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
            child: ListTile(
              leading: Icon(
                Icons.person,
                color: Colors.blue,
              ),
              title: Text(
                'Muhammad Prayoga Wicaksono',
                style: TextStyle(
                  color: Colors.blue.shade500,
                  fontFamily: 'Source Sans Pro',
                  fontSize: 20.0,
                ),
              ),
            ),
          ),
          Card(
            margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
            child: ListTile(
              leading: Icon(
                Icons.badge,
                color: Colors.blue,
              ),
              title: Text(
                '031200024',
                style: TextStyle(
                  color: Colors.blue.shade500,
                  fontFamily: 'Source Sans Pro',
                  fontSize: 20.0,
                ),
              ),
            ),
          ),
          Card(
            margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
            child: ListTile(
              leading: Icon(
                Icons.auto_stories,
                color: Colors.blue,
              ),
              title: Text(
                'D3 Sistem Informasi',
                style: TextStyle(
                  color: Colors.blue.shade500,
                  fontFamily: 'Source Sans Pro',
                  fontSize: 20.0,
                ),
              ),
            ),
          ),
          Card(
            margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
            child: ListTile(
              leading: Icon(
                Icons.calendar_month,
                color: Colors.blue,
              ),
              title: Text(
                'Palembang, 02 November 2002',
                style: TextStyle(
                  color: Colors.blue.shade500,
                  fontFamily: 'Source Sans Pro',
                  fontSize: 20.0,
                ),
              ),
            ),
          ),
          Card(
            margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
            child: ListTile(
              leading: Icon(
                Icons.male,
                color: Colors.blue,
              ),
              title: Text(
                'Laki-Laki',
                style: TextStyle(
                  color: Colors.blue.shade500,
                  fontFamily: 'Source Sans Pro',
                  fontSize: 20.0,
                ),
              ),
            ),
          ),
          Card(
            margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
            child: ListTile(
              leading: Icon(
                Icons.mosque,
                color: Colors.blue,
              ),
              title: Text(
                'Islam',
                style: TextStyle(
                  color: Colors.blue.shade500,
                  fontFamily: 'Source Sans Pro',
                  fontSize: 20.0,
                ),
              ),
            ),
          ),
          Card(
            margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
            child: ListTile(
              leading: Icon(
                Icons.phone,
                color: Colors.blue,
              ),
              title: Text(
                '+62 822 8120 9442',
                style: TextStyle(
                  color: Colors.blue.shade500,
                  fontFamily: 'Source Sans Pro',
                  fontSize: 20.0,
                ),
              ),
            ),
          ),
          Card(
            margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
            child: ListTile(
              leading: Icon(
                Icons.mail,
                color: Colors.blue,
              ),
              title: Text(
                'yogaw2000@gmail.com',
                style: TextStyle(
                  color: Colors.blue.shade500,
                  fontFamily: 'Source Sans Pro',
                  fontSize: 20.0,
                ),
              ),
            ),
          ),
          SizedBox(
            child: Divider(),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Ink(
                decoration: ShapeDecoration(
                  color: Color.fromARGB(255, 0, 81, 255),
                  shape: CircleBorder(),
                ),
                child: IconButton(
                  color: Colors.white,
                  onPressed: () {
                    Fluttertoast.showToast(
                      msg: "Kunjungi Facebook saya Muhammad Prayoga Wicaksono",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.CENTER,
                      timeInSecForIosWeb: 3,
                      backgroundColor: Colors.white,
                      textColor: Colors.white,
                      fontSize: 16.0,
                    );
                  },
                  icon: Icon(
                    Icons.facebook,
                  ),
                ),
              ),
              Ink(
                decoration: ShapeDecoration(
                  color: Color.fromARGB(255, 255, 103, 154),
                  shape: CircleBorder(),
                ),
                child: IconButton(
                  color: Colors.white,
                  onPressed: () {
                    Fluttertoast.showToast(
                      msg: "Kunjungi Instagram saya @prayogaa.w",
                      toastLength: Toast.LENGTH_SHORT,
                      gravity: ToastGravity.CENTER,
                      timeInSecForIosWeb: 3,
                      backgroundColor: Colors.green,
                      textColor: Colors.white,
                      fontSize: 16.0,
                    );
                  },
                  icon: Icon(
                    Icons.square_outlined,
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
